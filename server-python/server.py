from flask import Flask
from flask_cors import CORS, cross_origin

import json

app = Flask("Flask Reddit server")
CORS(app)

@app.route("/")
def say_hello():
    return "Hello world"


@app.route("/api/v1/reddit_feed")
def get_reddit_data():
    children_object = {
        "children": [
            {"data": {"title": "First Article", 
                        "ups": 123} 
             }, 
             {"data": {"title": "Second Article", 
                        "ups": 60} 
             }
        ]
    }
    json_response = {}
    json_response["data"] = children_object
    return json.dumps(json_response)

app.run(port=5000, debug=True)